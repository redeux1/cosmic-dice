# Changelog
## 1.2.1
- Added Cosmic Ring standard token assets (for non-dynamic ring use)

## 1.2.0
- Updated dynamic ring to include 'ringThickness' attribute to make use of Foundry's "Grid Fit" mode (Thank you Foundry Staff!)
- Added Cosmic Ring screenshot to readme

## 1.1.2
- Added back module thumbnail which was inadvertently dropped in new packaging pipeline
- Reorganized folder structure

## 1.1.0
- Added a Cosmic Dynamic Ring

## 1.0.22
- Built Pipeline to make prior versions more accessible. In preparation for minimum v12 version

## 1.0.2
- Verified v12 compatibility

##  1.0.1
- Verified v11 compatibility

## 1.0.0
- Initial release