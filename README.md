# Cosmic Dice

Sci-Fi themed virtual dice sets and dynamic ring to use within FoundryVTT for anyone to use. A special thanks to the the Game Master's of the Cosmic Crit [discord server](https://discord.gg/hAQgjmX). Thank you for all that you do for our community! 

![A screenshot of the cosmic dice set with a starfield background. The dice are purple with lime green, sci-fi numbers and have a metallic finish. ](/screenshots/Dice.webp "Cosmic Dice set")

![A screenshot of the cosmic dynamic ring with a starfield background. The ring is purple with a starfield background itself.](/screenshots/ring.webp "Cosmic Dynamic Ring")

## Installation

Install via Foundry's Install A Module Menu.

## Usage

- Dice: Make sure both Dice so Nice and Cosmic Dice modules are installed and active. Go to Dice so Nice settings and select one of the Cosmic dice sets under "Dice Presets" 

- Cosmic Ring: 
  - (Dynamic Ring) Under Core settings find the Dynamic Token Rings section and select "Cosmic Ring". Optionally, set the 'Dynamic Rings Token Fit Mode' to 'Grid Fit' if you want the rings to fit to the grid. 
  - (Standard Token) Alternatively, in module assets there are standard token assets for non-dynamic use. Note that the original ring was designed without the inner-most purple ring and is included as a separate (optional) layer. 

## Feedback

If you have any suggestions or feedback, please contact redeux on the Cosmic Crit discord server, or FoundryVTT [discord server](https://discord.gg/foundryvtt). Alternatively, you can create an issue. 

## Licensing

**Project Licensing**

- All HTML, CSS and Javascript in this project is licensed under the MIT License.

**Content Usage and Licensing:**
- Cosmic Crit's logo provided by GM Patrick and used with permission. If you aren't familiar with Cosmic Crit then check out [their Starfinder Actual Play podcast](https://www.cosmiccrit.com/)!
- Dice textures were created and developed by redeux for exclusive use within this project.
- Font used is Krypton, by [Pinisiart](https://www.1001fonts.com/krypton-font.html), via commercial license.
- Token ring assets sourced from REXARD and are licensed under the [Rexard Game Dev Assets EULA](https://support.humblebundle.com/hc/en-us/articles/360046217533-Rexard-Game-Dev-Assets-EULA-).
- Token and screenshot background (starfield) from [textures4photoshop](https://www.textures4photoshop.com/tex/clouds-and-sky/space-background-with-starfield-free-download.aspx). 
- Monster art for Dynamic Ring screenshot: [Basalt Crab](https://2minutetabletop.com/product/colossal-crab/), by 2-Minute Tabletop. There is also a [Colossal Crab Pack](https://2minutetabletop.com/product/colossal-crab-pack/) worth checking out. 

**Virtual Table Top Platform Licenses:**

- This module for Foundry Virtual Tabletop is licensed under the [Limited License Agreement for Module Development](https://foundryvtt.com/article/license/).

## Patch Notes

See [CHANGELOG.md](./CHANGELOG.md)