//hook to add a new texture before the colorset
Hooks.once('diceSoNiceInit', (dice3d) => {
	/**
	 * Add a texture to the list of textures and preload it
	 * @param {String} textureID
	 * @param {Object} textureData {name,composite,source}
	 * @returns {Promise}
	 **/

	dice3d.addTexture("cosmicPurple", {
		name: "Cosmic Purple",
		composite: "source-over",
		source: "modules/cosmic-dice/assets/textures/cosmicPurple.webp"
	});

	dice3d.addTexture("cosmicCustom", {
		name: "Cosmic Custom",
		composite: "source-over",
		source: "modules/cosmic-dice/assets/textures/cosmicCustom.webp"
	});
});

//hook to put create color set and dice system in to DsN options.
Hooks.once('diceSoNiceReady', (dice3d) => {
	/**
 	* Add a colorset (theme)
 	* @param {Object} colorset (see below)
	* @param {String} mode= "default","preferred"
 	* The "mode" parameter have 2 modes : 
 	* - "default" only register the colorset
 	* - "preferred" apply the colorset if the player didn't already change his dice appearance for this world. 
	**/

	dice3d.addColorset({
		name: 'CosmicPurpleSet',
		description: "Cosmic Purple",
		category: "Cosmic Dice",
		texture: "cosmicPurple",
		material: "metal",
		foreground: '#39f22c',
		background: '#8013c9',
		outline: '#000000',
		//edge: '#8013c9',
		font: "Krypton",
		visibility: 'hidden'
	},"default");

	dice3d.addColorset({
		name: 'CosmicCustomSet',
		description: "Cosmic Custom",
		category: "Cosmic Dice",
		texture: "cosmicCustom",
		material: "metal",
		//foreground: '#000000',
		//background: '#000000',
		//outline: '#000000',
		//edge: '#000000',
		font: "Krypton",
		visibility: 'hidden'
	},"default");
	
	//Set up the system.
	dice3d.addSystem({id:"CDPurple",name:"Cosmic Purple"},false);
	dice3d.addSystem({id:"CDCustom",name:"Cosmic Customize Colors"},false);
	dice3d.addSystem({id:"CDPurpleUnbranded",name:"Cosmic Purple (Unbranded) "},false);
	dice3d.addSystem({id:"CDCustomUnbranded",name:"Cosmic Customize Colors (Unbranded)"},false);

	// do this function once for each .glb file within ./dice folder.
	// can be achieved via a loop, assuming file names are like d2.glb, d4.glb, etc.
	// modelFile: modules/<module name>/dice/filename
	// type: <filename without extension>
	// system: grab from id above.
	
	//CosmicPurple
	dice3d.addDicePreset({
		type:"df",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		fontScale: "3",
		labels:[
		  '-',' ','+'
		],
		system: "CDPurple"
	  });

	dice3d.addDicePreset({
	  type:"dc",
	  colorset: "CosmicPurpleSet",
	  fontScale: "0.5",
	  font: "Krypton",
	  labels:[
		'PRONK!', 'modules/cosmic-dice/assets/faces/dc.webp'
	  ],
	  system:"CDPurple"
	});
	  
	dice3d.addDicePreset({
		type:"d2",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2'
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d4",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4'
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d6",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6',
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d8",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8',
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d10",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10',
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d100",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '10','20','30','40','50','60','70','80','90','00',
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d12",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12',
		],
		system:"CDPurple"
	  });
	
	dice3d.addDicePreset({
		type:"d20",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','modules/cosmic-dice/assets/faces/d20.webp',
		],
		system:"CDPurple"
	  });

	dice3d.addDicePreset({
		type:"d3",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3'
		],
		system:"CDPurple"
	  });
	  
	dice3d.addDicePreset({
		type:"d5",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5'
		],
		system:"CDPurple"
	  });

	dice3d.addDicePreset({
		type:"d7",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7'
		],
		system:"CDPurple"
	  });

	dice3d.addDicePreset({
		type:"d14",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14'
		],
		system:"CDPurple"
	  });

	dice3d.addDicePreset({
		type:"d16",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16'
		],
		system:"CDPurple"
	  });

	dice3d.addDicePreset({
		type:"d24",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'
		],
		system:"CDPurple"
	  });

	dice3d.addDicePreset({
		type:"d30",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30'
		],
		system:"CDPurple"
	  });

	  //CustomCosmic
	dice3d.addDicePreset({
		type:"df",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		fontScale: "3",
		labels:[
		  '-',' ','+'
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"dc",
		colorset: "CosmicCustomSet",
		fontScale: "0.5",
		font: "Krypton",
		labels:[
		  'PRONK!', 'modules/cosmic-dice/assets/faces/dc.webp'
		],
		system:"CDCustom"
	  });
	  
	dice3d.addDicePreset({
		type:"d2",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2'
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d4",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4'
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d6",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6',
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d8",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8',
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d10",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10',
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d100",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '10','20','30','40','50','60','70','80','90','00',
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d12",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12',
		],
		system:"CDCustom"
	  });
	
	dice3d.addDicePreset({
		type:"d20",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','modules/cosmic-dice/assets/faces/d20.webp',
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"d3",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3'
		],
		system:"CDCustom"
	  });
	  
	dice3d.addDicePreset({
		type:"d5",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5'
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"d7",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7'
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"d14",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14'
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"d16",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16'
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"d24",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'
		],
		system:"CDCustom"
	  });

	dice3d.addDicePreset({
		type:"d30",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30'
		],
		system:"CDCustom"
	  });

	  //CosmicPurpleUnbranded
	dice3d.addDicePreset({
		type:"df",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		fontScale: "3",
		labels:[
		  '-',' ','+'
		],
		system:"CDPurpleUnbranded"
	  });
	  
	dice3d.addDicePreset({
		type:"d2",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2'
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d4",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4'
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d6",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6',
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d8",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8',
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d10",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10',
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d100",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '10','20','30','40','50','60','70','80','90','00',
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d12",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12',
		],
		system:"CDPurpleUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d20",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20',
		],
		system:"CDPurpleUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d3",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3'
		],
		system:"CDPurpleUnbranded"
	  });
	  
	dice3d.addDicePreset({
		type:"d5",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5'
		],
		system:"CDPurpleUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d7",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7'
		],
		system:"CDPurpleUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d14",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14'
		],
		system:"CDPurpleUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d16",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16'
		],
		system:"CDPurpleUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d24",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'
		],
		system:"CDPurpleUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d30",
		colorset: "CosmicPurpleSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30'
		],
		system:"CDPurpleUnbranded"
	  });

	  //CustomCosmicUnbranded
	dice3d.addDicePreset({
		type:"df",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		fontScale: "3",
		labels:[
		  '-',' ','+'
		],
		system:"CDCustomUnbranded"
	  });
	  
	dice3d.addDicePreset({
		type:"d2",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2'
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d4",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4'
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d6",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6',
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d8",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8',
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d10",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10',
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d100",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '10','20','30','40','50','60','70','80','90','00',
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d12",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12',
		],
		system:"CDCustomUnbranded"
	  });
	
	dice3d.addDicePreset({
		type:"d20",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20',
		],
		system:"CDCustomUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d3",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3'
		],
		system:"CDCustomUnbranded"
	  });
	  
	dice3d.addDicePreset({
		type:"d5",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5'
		],
		system:"CDCustomUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d7",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7'
		],
		system:"CDCustomUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d14",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14'
		],
		system:"CDCustomUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d16",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16'
		],
		system:"CDCustomUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d24",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'
		],
		system:"CDCustomUnbranded"
	  });

	dice3d.addDicePreset({
		type:"d30",
		colorset: "CosmicCustomSet",
		font: "Krypton",
		labels:[
		  '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30'
		],
		system:"CDCustomUnbranded"
	  });
});