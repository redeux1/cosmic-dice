// Create a hook to add a custom token ring configuration. This ring configuration will appear in the settings.
Hooks.on('initializeDynamicTokenRingConfig', (ringConfig) => {
  const cosmicRing = new foundry.canvas.tokens.DynamicRingData({
    label: 'Cosmic Ring',
    effects: {
      RING_PULSE: 'TOKEN.RING.EFFECTS.RING_PULSE',
      RING_GRADIENT: 'TOKEN.RING.EFFECTS.RING_GRADIENT',
      BKG_WAVE: 'TOKEN.RING.EFFECTS.BKG_WAVE',
      INVISIBILITY: 'TOKEN.RING.EFFECTS.INVISIBILITY',
    },
    spritesheet: 'modules/cosmic-dice/assets/rings/sprite-sheet.json',
  });
  ringConfig.addConfig('cosmicRing', cosmicRing);
});
